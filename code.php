<?php
// [Activity 1]
function getFullAddress ($country, $city, $province, $specificAddress) {
	return "$specificAddress, $city ,$province ,$country";
}
// [Activity 2]
function getLetterGrade ($grade) {
	if($grade >= 98){
		return "$grade is equivalent to A+ ";
	}else if($grade >= 95){
		return "$grade is equivalent to A ";
	}else if($grade >= 92){
		return "$grade is equivalent to A- ";
	}else if($grade >= 89){
		return "$grade is equivalent to B+ ";
	}else if($grade >= 86){
		return "$grade is equivalent to B ";
	}else if($grade >= 83){
		return "$grade is equivalent to B- ";
	}else if($grade >= 80){
		return "$grade is equivalent to C+ ";
	}else if($grade >= 77){
		return "$grade is equivalent to C ";
	}else if($grade >= 75){
		return "$grade is equivalent to C- ";
	}else{
		return "$grade is equivalent to D ";
	}
}

?>