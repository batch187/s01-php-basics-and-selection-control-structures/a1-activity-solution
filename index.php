<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity 1 PHP</title>
</head>
<body>
	<!-- [Activity 1] -->
	<h1>Full Address</h1>
	<p><?php echo getFullAddress("Philippines", "Quezon City", "Metro Manila", "3F Caswynn Bldg"); ?></p>
	<p><?php echo getFullAddress("Philippines", "Makati City", "Metro Manila", "3F Enzo Bldg"); ?></p>
	
	<!-- [Activity 2] -->
	<h1>Letter Based Grading</h1>
	<p><?php echo getLetterGrade('87') ?></p>
	<p><?php echo getLetterGrade('94') ?></p>
	<p><?php echo getLetterGrade('74') ?></p>

</body>
</html>